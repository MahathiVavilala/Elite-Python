from math import sqrt
n = 4000000
primesList = [primes for primes in range(2, int(sqrt(n))) if 0 not in [primes % var for var in range(2, int(primes / 2 + 1))]]
print(primesList[1000])
