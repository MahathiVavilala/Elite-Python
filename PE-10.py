import math
primelist = [x for x in range(2, 20000000) if all(x % y != 0 for y in range(2, int(math.sqrt(x)) + 1))]
print(primelist)
