from math import sqrt
n = 600851475143 
PrimeFactors = []
for i in [primes for primes in range(2, int(sqrt(n))) if 0 not in [primes % var for var in range(2, int(primes / 2 + 1))]]:
    if n % i == 0:
        PrimeFactors.append(i)
print(PrimeFactors[-1])
